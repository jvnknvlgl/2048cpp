#ifndef BOARD_HPP
#define BOARD_HPP

#include <array>
#include <functional>
#include <random>
#include <tuple>

#define BOARD_SIZE 4

enum Direction {
    LEFT,
    DOWN,
    RIGHT,
    UP,
};

class Board {
  public:
    void reset_board();
    std::array<std::array<int, BOARD_SIZE>, BOARD_SIZE> get_board();
    std::pair<int, int> get_score();
    void handle_input(char inp, bool &quit);
    void play_move(int dir);
    bool check_game_over();

  private:
    friend class IO;

    std::default_random_engine generator;
    std::array<std::array<int, BOARD_SIZE>, BOARD_SIZE> board{};
    int score = 0;
    int highscore = 0;
    bool board_changed = false;

    void add_random_tile();
    void rotate_board();
    void move_tiles(int i);
    void add_tiles();
    bool has_board_changed();
    bool free_tile();
    bool game_over();
};

#endif
