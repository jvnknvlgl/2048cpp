#include "Board.hpp"

void Board::reset_board() {
    score = 0;

    for (std::array<int, BOARD_SIZE> &i : board)
        for (int &j : i)
            j = 0;

    add_random_tile();
}

std::array<std::array<int, BOARD_SIZE>, BOARD_SIZE> Board::get_board() {
    return board;
}

std::pair<int, int> Board::get_score() {
    return std::make_pair(score, highscore);
}

void Board::handle_input(char inp, bool &quit) {
    switch (inp) {
    case 'w':
        play_move(Direction::UP);
        break;
    case 'a':
        play_move(Direction::LEFT);
        break;
    case 's':
        play_move(Direction::DOWN);
        break;
    case 'd':
        play_move(Direction::RIGHT);
        break;
    case 'n':
        reset_board();
        break;
    case 'q':
        quit = true;
        break;
    default:
        break;
    }
}

void Board::play_move(int dir) {
    auto prev_board = board;

    for (int i = 0; i < dir; i++)
        rotate_board();

    add_tiles();

    for (int j = 0; j < (4 - dir); j++)
        rotate_board();

    if (prev_board != board) {
        board_changed = true;
        add_random_tile();
    } else
        board_changed = false;
}

bool Board::check_game_over() {
    return (!has_board_changed() && !free_tile() && game_over());
}

void Board::add_random_tile() {
    static std::uniform_int_distribution<int> tile_dist(0, BOARD_SIZE - 1);
    static std::uniform_int_distribution<int> value_dist(0, 10);
    static auto random_tile = std::bind(tile_dist, generator);
    static auto random_value = std::bind(value_dist, generator);

    int n = 0;

    while (n < 100) {
        int i = random_tile();
        int j = random_tile();

        if (board[i][j] == 0) {
            if (random_value() == 0) {
                board[i][j] = 4;
                break;
            } else {
                board[i][j] = 2;
                break;
            }
        } else
            n++;
    }
}

void Board::rotate_board() {
    std::array<std::array<int, BOARD_SIZE>, BOARD_SIZE> rotated_board = {{{0}}};

    for (int i = 0; i < BOARD_SIZE; i++)
        for (int j = 0; j < BOARD_SIZE; j++)
            rotated_board[i][j] = board[BOARD_SIZE - 1 - j][i];

    board = rotated_board;
}

void Board::move_tiles(int i) {
    for (int n = 0; n < BOARD_SIZE + 1; n++)
        for (int j = 0; j < BOARD_SIZE - 1; j++)
            if (board[i][j] == 0) {
                board[i][j] = board[i][j + 1];
                board[i][j + 1] = 0;
            }
}

void Board::add_tiles() {
    for (int i = 0; i < 4; i++) {
        move_tiles(i);

        for (int j = 0; j < 3; j++)
            if (board[i][j] == board[i][j + 1] && board[i][j] != 0) {
                board[i][j] = 2 * board[i][j];
                board[i][j + 1] = 0;
                score = score + board[i][j];
                if (highscore < score)
                    highscore = score;
            }

        move_tiles(i);
    }
}

bool Board::has_board_changed() { return board_changed; }

bool Board::free_tile() {
    for (std::array<int, BOARD_SIZE> &i : board)
        for (int &j : i)
            if (j == 0)
                return true;

    return false;
}

bool Board::game_over() {
    auto prev_board = board;
    auto prev_score = score;

    for (int i = 0; i < 4; i++) {
        play_move(i);

        if (board != prev_board) {
            score = prev_score;
            board = prev_board;

            return false;
        }
    }

    return true;
}
