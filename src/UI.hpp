#ifndef UI_HPP
#define UI_HPP

#include <array>
#include <string>

#ifdef _WIN32
#include <iostream>
#else
#include <curses.h>
#endif

#include "Board.hpp"

class UI {
  public:
    UI();
    ~UI();

    void set_board(std::array<std::array<int, BOARD_SIZE>, BOARD_SIZE> board);
    void set_scores(int score, int highscore);
    void refresh();
    void game_over();
    char get_user_input();

  private:
    std::array<std::array<int, BOARD_SIZE>, BOARD_SIZE> ui_board{};
    int ui_score{}, ui_highscore{};
#ifdef _WIN32
#else
    int max_x = 0;
    int max_y = 0;

    WINDOW *game_board = NULL;
    WINDOW *user_input = NULL;
#endif
};

#endif
