#include "UI.hpp"

UI::UI() {
#ifdef _WIN32
#else
    initscr();
    curs_set(false);

    getmaxyx(stdscr, max_y, max_x);

    game_board = newwin(11, max_x, 0, 0);
    user_input = newwin(1, max_x, 12, 0);
#endif
}

UI::~UI() {
#ifdef _WIN32
#else
    delwin(game_board);
    delwin(user_input);

    endwin();
#endif
}

void UI::set_board(std::array<std::array<int, BOARD_SIZE>, BOARD_SIZE> board) {
    ui_board = board;
}

void UI::set_scores(int score, int highscore) {
    ui_score = score;
    ui_highscore = highscore;
}

void UI::refresh() {
    std::string score_str("\tScore:\t\t ");
    std::string highscore_str("\tHigh score:\t ");
    std::string control_str("\tPress [w/a/s/d] or [n/q] ");

#ifdef _WIN32
    std::cout << std::endl;

    for (const std::array<int, BOARD_SIZE> &i : ui_board) {
        for (const int &j : i) {
            if (j == 0)
                std::cout << "\t." << std::flush;
            else
                std::cout << "\t" << j << std::flush;
        }

        std::cout << "\n" << std::endl;
    }

    std::cout << score_str << ui_score << std::endl;
    std::cout << highscore_str << ui_highscore << "\n" << std::endl;
    std::cout << control_str << std::flush;
#else
    wclear(user_input);
    wclear(game_board);
    wprintw(game_board, "\n");

    for (const std::array<int, BOARD_SIZE> &i : ui_board) {
        for (const int &j : i) {
            if (j == 0)
                wprintw(game_board, "\t.");
            else
                wprintw(game_board, "\t%d", j);
        }

        wprintw(game_board, "\n\n");
    }

    wprintw(game_board, "%s %d\n", score_str.c_str(), ui_score);
    wprintw(game_board, "%s %d\n", highscore_str.c_str(), ui_highscore);
    wprintw(user_input, "%s", control_str.c_str());

    wrefresh(user_input);
    wrefresh(game_board);
#endif
}

void UI::game_over() {
    std::string game_over("Game over! Press any button to reset.");
#ifdef _WIN32
    std::cout << game_over << std::endl;
#else
    wclear(user_input);
    wprintw(user_input, "%s", game_over.c_str());
    wrefresh(user_input);
#endif
}

char UI::get_user_input() {
    char inp;

#ifdef _WIN32
    std::cin >> inp;
#else
    inp = static_cast<char>(wgetch(user_input));
#endif

    return inp;
}
