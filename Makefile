EXE = 2048

SRC_DIR = src
OBJ_DIR = obj

SRC = $(wildcard $(SRC_DIR)/*.cpp)
OBJ = $(SRC:$(SRC_DIR)/%.cpp=$(OBJ_DIR)/%.o)

LDLIBS = $(shell pkg-config --libs ncurses) -lstdc++
CFLAGS = $(shell pkg-config --cflags ncurses) \
	-Wall -Werror -Wextra -std=c++20 -O3

all: $(EXE)

$(EXE): $(OBJ)
	$(CC) $^ $(LDLIBS) -o $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	@mkdir -p $(OBJ_DIR)
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(RM) $(OBJ) $(EXE)

run: $(EXE)
	./$(EXE)
