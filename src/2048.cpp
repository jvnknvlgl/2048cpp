#include <tuple>

#include "Board.hpp"
#include "IO.hpp"
#include "UI.hpp"

int main() {
    Board game;
    UI ui;
    IO io("save", "jvnknvlgl");

    int score, highscore;
    bool quit = false;

    if (io.get_file_size() > 0)
        io.load_board(game);
    else
        game.reset_board();

    while (!quit) {
        auto board = game.get_board();
        std::tie(score, highscore) = game.get_score();

        ui.set_board(board);
        ui.set_scores(score, highscore);
        ui.refresh();

        char inp = ui.get_user_input();
        game.handle_input(inp, quit);

        if (game.check_game_over()) {
            ui.game_over();
            ui.get_user_input();
            game.reset_board();
        }
    }

    io.save_board(game);
}
