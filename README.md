# 2048.cpp

CLI-based reimplementation of 2048, written in C++. Supports saving to a local text file. Supports a ncurses interface where supported and an iostream interface on Windows, where the former can be played without pressing enter after every move.

## Building and running

Clone the repository, navigate to the directory and compile with `make`. The game can be played with `./2048` or `make run`. No `make install` is supported as of right now. The game data is saved in the file `save`, if this file is deleted the game is reset.

## To do list

- Add a proper location to save the game file, for example using the XDG directory standard.
- Add a better way to set the encryption key, right now it is really only obfuscation. Possibly based off the username of the player.

## Support

The game is developed on the most recent version of Fedora Silverblue. Any other operating system might work, provided they ship a C++ compiler and the required development headers (mostly ncurses, everything else should be in the standard libary). macOS, for example, should work just fine. Note that the required implementation of `make` is GNU Make, so one would have to use `gmake` to build on most BSDs. The game is confirmed to work on the following platforms:

- Fedora 37
- FreeBSD 13
- macOS 13
- Windows 10
