#include "IO.hpp"

#include <utility>

IO::IO(std::string path, std::string key)
    : path(std::move(path)), key(std::move(key)) {}

off_t IO::get_file_size() {
    struct stat stat_buf {};
    off_t rc = stat(path.c_str(), &stat_buf);
    return rc == 0 ? stat_buf.st_size : -1;
}

void IO::load_board(Board &game) {
    std::ifstream save;
    save.open(path);

    std::vector<int> data;
    int line, i = 0, j = 0;

    while (save >> line)
        data.push_back(line);

    decrypt(data);

    game.score = data[0];
    game.highscore = data[1];

    for (size_t k = 2; k < data.size(); k++) {
        game.board[i][j] = data[k];

        if (j == BOARD_SIZE - 1) {
            j = 0;
            i++;
        } else
            j++;
    }

    save.close();
}

void IO::save_board(Board &game) {
    std::ofstream save;
    save.open(path);

    std::vector<int> data;
    data.push_back(game.score);
    data.push_back(game.highscore);

    for (const std::array<int, BOARD_SIZE> &i : game.board)
        for (const int &j : i)
            data.push_back(j);

    encrypt(data);

    for (const int &i : data)
        save << i << std::endl;

    save.close();
}

void IO::decrypt(std::vector<int> &data) {
    for (size_t i = 0; i < data.size(); i++)
        data[i] ^= key[i % key.size()];
}

void IO::encrypt(std::vector<int> &data) {
    for (size_t i = 0; i < data.size(); i++)
        data[i] ^= key[i % key.size()];
}
