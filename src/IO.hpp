#ifndef IO_HPP
#define IO_HPP

#include <array>
#include <fstream>
#include <string>
#include <vector>

#include <sys/stat.h>

#include "Board.hpp"

class IO {
  public:
    IO(std::string path, std::string key);

    off_t get_file_size();
    void load_board(Board &game);
    void save_board(Board &game);

  private:
    std::string path;
    std::string key;

    void decrypt(std::vector<int> &data);
    void encrypt(std::vector<int> &data);
};

#endif
